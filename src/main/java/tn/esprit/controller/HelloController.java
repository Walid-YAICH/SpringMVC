/*
 * Copyright 2017 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import tn.esprit.dto.MessageDTO;

/**
 * lorsque je déploi manuellement (package, puis, je mets le jar dans le tomcat), c'est cet url :
 * http://localhost:8082/helloWorld/servlet/sayHello?myName=Walid
 * helloWorld est le nom du jar
 * @author yaich
 *
 */
@Controller
public class HelloController {

	//@RequestMapping("/sayHello") //can use @GetMapping as well
	@GetMapping("/sayHello")
	public String sayIt(Model model, 
						@RequestParam("myName") String name){
		model.addAttribute("receivedName", name);
		return "helloPage";
	}
	
	//Ceci est un endpoint rest
	//http://localhost:8082/helloWorld/servlet/sayHelloRest?myName=Walid
	//No need for @ResponseBody if you use @RestController
	@GetMapping("/sayHelloRest")
	public @ResponseBody MessageDTO sayItHello(@RequestParam("myName") String name) {
		return new MessageDTO("Hello " + name);				
	}
	
}

