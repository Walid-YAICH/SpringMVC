/*
 * Copyright 2017 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.config.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@ComponentScan(basePackages={"tn.esprit.config.web", "tn.esprit.controller"})
@EnableWebMvc //j'ai rajouté cette annotation  suite a cette erreur 
//HttpMessageNotWritableException: No converter found for return value of type: class tn.esprit.dto.MessageDTO
//cette erreur est apparue lorsque j'ai voulu rajouté un endpoint restful
//==> il n'a pas pu convertir ma réponse (l'objet MessageDTO) vers JSON a travers HTTP.
//Attention, il faut aussi rajouter jackson dependencies dans le classpath
public class MvcConfig {

	@Bean
	public ViewResolver simpleViewResolver(){
		InternalResourceViewResolver vr = new InternalResourceViewResolver();
		vr.setPrefix("/views/"); //Relatif a webapp
		vr.setSuffix(".jsp");
		return vr;
	}
}




