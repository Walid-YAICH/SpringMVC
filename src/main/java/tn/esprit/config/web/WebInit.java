package tn.esprit.config.web;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import tn.esprit.config.RootConfig;

/**
 * Cette classe permet de deployer le DispatcherServlet
 * Avec cette classe, on n'a plus besoin du web.xml
 * 
 * cette classe implémente l'interface WebApplicationInitializer
 * Doc : WebApplicationInitializer to register a DispatcherServlet and use Java-based Spring configuration. 
 * 
 * SpringServletContainerInitializer implémente l'interface ServletContainerInitializer de la spec Servlet 3
 * 
 * SpringServletContainerInitializer calls WebApplicationInitializer
 * 
 * ==> l'invocation automatique de ServletContainerInitializer va deployer le dispatcherServlet
 * 
 * @author Walid YAICH
 *
 */
public class WebInit extends AbstractAnnotationConfigDispatcherServletInitializer {

	//getRootConfigClasses() -- for "root" application context (non-web infrastructure) configuration. 
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class<?>[]{RootConfig.class};
	}

	//getServletConfigClasses() -- for DispatcherServlet application context (Spring MVC infrastructure) configuration. 
	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[]{MvcConfig.class};
	}

	@Override
	protected String[] getServletMappings() {
		return new String[]{"/servlet/*"};
	}

}